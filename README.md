## Laboratory management
This prgm makes it possible to identify the people who work in the laboratory and,
  more precisely, to know which office they occupy

## This prgm has the following features:
* register the arrival of a new person.
* register the departure of a person
* change the office occupied by a person
* correct the name of the person (in case of bad spelling, etc.)
* know if a person is a member of the laboratory
* know the office of a person
* produce the list of all staff with the office occupied
* save data in a save.txt file

A text menu provides access to the various functions
the functionalities are listed in a second python file named lab_ges.py

## Prerequisites
Python3

## Launch
python3 lab_main.py