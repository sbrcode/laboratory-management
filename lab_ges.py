import os, re

class LaboException(Exception):
    pass
class PresentException(LaboException):
    pass
class AbsentException(LaboException):
    pass
class BureauException(LaboException):
    pass

def _verifier_present(nom):
	if nom in labo:
		raise PresentException
        
def _verifier_absent(nom):
    if nom not in labo:
        raise AbsentException

def _verifier_bureau(bureau):
    if not re.match(r"[A-C][0-9][0-9]", bureau):
        raise BureauException

msg1 = "Membre déjà existant !"
msg2 = "Ce membre n'existe pas !"
msg3 = "Bureau inexistant ! Il doit être de la forme [A-C][0-9][0-9]"

labo = None
util = input("voulez-vous utiliser la dernière sauvegarde ? (o/n) ")
if os.path.isfile('sauve_labo.txt') and util.lower() == "o":
    with open('sauve_labo.txt', 'r') as fichier:
        exec(fichier.read())
else:
    labo = {}

def enregistrerarrivee(nom, bureau):
    try:
        _verifier_present(nom)
        _verifier_bureau(bureau)
        labo[nom] = bureau
    except PresentException:
        print("--> "+nom+":\t"+msg1)
    except BureauException:
        print("--> "+bureau+":\t"+msg3)

def enregistrerdepart(nom):
    try:
        _verifier_absent(nom)
        del labo[nom]
    except AbsentException:
        print("--> "+nom+":\t"+msg2)

def modifiervaleur(nom, bureau):
    try:
        _verifier_absent(nom)
        _verifier_bureau(bureau)
        labo[nom] = bureau
    except AbsentException:
        print("--> "+nom+":\t"+msg2)
    except BureauException:
        print("--> "+bureau+":\t"+msg3)

def modifierclef(nom, nouvnom):
    try:
        _verifier_absent(nom)
        _verifier_present(nouvnom)
        labo[nouvnom] = labo[nom]
        del labo[nom]
    except AbsentException:
        print("--> "+nom+":\t"+msg2)
    except PresentException:
        print("--> "+nouvnom+":\t"+msg1)

def rechercherclef(nom):
    try:
        _verifier_present(nom)
        _verifier_absent(nom)
    except AbsentException:
        print("--> "+nom+":\t"+msg2)
    except PresentException:
        print("--> "+nom+":\t"+msg1)

def recherchervaleur(nom):
    try:
        _verifier_absent(nom)
        bureau = labo.get(nom)
        print("--> Bureau"+":\t"+labo.get(nom))
    except AbsentException:
        print("--> "+nom+":\t"+msg2)

def listerlabo():
    if len(labo) != 0:
        print("Membre\tBureau")
        for nom,bureau in labo.items():
            print("{}\t{}".format(nom,bureau))

def listerbureaux():
    if len(labo) != 0:
        enslabo = set()
        listenoms = []
        listebureaux = []
        for nom, bureau in labo.items():
            enslabo.add(bureau)
            listenoms.append(nom)
            listebureaux.append(bureau)
        print("Bureau\tOccupants")
        for bureau in sorted(enslabo):
            occupants = []
            for indice in range(len(listebureaux)):
                if listebureaux[indice] == bureau:
                    occupants.append(listenoms[indice])
            print("{}\t{}".format(bureau," ; ".join(occupants)))

def sauverlabo():
    with open('sauve_labo.txt', 'w') as fichier:
        a_sauver = "labo = {}".format(labo)
        fichier.write(a_sauver)        