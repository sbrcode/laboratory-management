from os import system
from lab_ges import *

choix = None
system('clear')

main_menu = [
    ('0', "Quitter", 'exit()'),
    ('1', "enregistrer l’arrivée d’une nouvelle personne", 'enregistrerarrivee(var1, var2)', 'Nom : ', 'Bureau : '),
    ('2', "enregistrer le départ d’une personne", 'enregistrerdepart(var)', 'Nom : '),
    ('3', "modifier le bureau occupé par une personne", 'modifiervaleur(var1, var2)', 'Nom : ', 'Bureau : '),
    ('4', "corriger le nom d'une personne", 'modifierclef(var1, var2)', 'Nom : ', 'Nouveau Nom : '),
    ('5', "savoir si une personne est membre du laboratoire", 'rechercherclef(var)', 'Nom : '),
    ('6', "connaitre le bureau d’une personne", 'recherchervaleur(var)', 'Nom : '),
    ('7', "produire le listing de tous les personnels avec le bureau occupé", 'listerlabo()'),
    ('8', "afficher l’occupation des bureaux", 'listerbureaux()'),
    ('9', "sauvegarder le labo créé", 'sauverlabo()')
            ]

def affiche_menu(menu):
    for ligne in menu:
        print("{} - {}".format(ligne[0], ligne[1]))
    choix = input("Que voulez faire svp: ")
    return choix

def exec_menu(choix, menu):
    for x in range(len(menu)):
        if choix == menu[x][0]:
            print("*cmd* {}".format(menu[x][1]))
            if len(menu[x]) == 5:
                var1 = input(menu[x][3])
                var2 = input(menu[x][4])
            elif len(menu[x]) == 4:
                var = input(menu[x][3])
            exec(menu[x][2])
    
print("Bienvenue dans le menu de gestion du laboratoire :")
if __name__ == "__main__":
    while choix != 0:
        exec_menu(affiche_menu(main_menu), main_menu)